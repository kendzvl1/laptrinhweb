package poly.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sun.xml.internal.org.jvnet.mimepull.MIMEMessage;

import poly.entity.Customer;
import poly.entity.Main;
import poly.entity.Oder;


@Controller
@Transactional
public class IndexController {
	@Autowired
	SessionFactory factory;
	@Autowired
	JavaMailSender mailer;
	
	@RequestMapping("action_page")
	public String action_page(ModelMap model, HttpServletRequest rq){
		try {
			
		
		String param = rq.getParameter("xsearch");
		Session ss = factory.getCurrentSession();
		String hql = "From Main where name LIKE '%"+param+"%'";
		Query query = ss.createQuery(hql);
		List<Main> list = query.list();
		System.out.println(list.size());
		model.addAttribute("combo",list);
		model.addAttribute("title","Tìm được "+list.size()+" kết quả cho "+param+"");
		}
		catch (Exception e) {
			System.out.println(e);
		}
		return "tab/combo";
	}
	
	@RequestMapping("trangchu")
	public String index(ModelMap model){
		try {
			
		
		Session session = factory.getCurrentSession();
		String hql = "FROM Main where info.idtype = 8";
		Query query = session.createQuery(hql);
		List<Main> list = query.list();
		
		model.addAttribute("combo",list);
		
		String hql2 = "FROM Main where info.idtype = 1";
		Query qr = session.createQuery(hql2);
		List<Main> list2 = qr.list();
		model.addAttribute("ram",list2);
		
		String hql3 = "FROM Main where info.idtype = 2";
		Query qrx = session.createQuery(hql3);
		List<Main> list3 = qrx.list();
		model.addAttribute("cpu",list3);
//		for(int i=0;i<list.size();i++){
//			System.out.println(list.get(i).getName());
//		}
		} catch (Exception e) {
			// TODO: handle exception
		}
//		
		return "tab/index";
	}
	
	
	@RequestMapping("combo")
	public String combo(ModelMap model){
		try {
			
		
		Session session = factory.getCurrentSession();
		String hql = "FROM Main where info.idtype = 8";
		Query query = session.createQuery(hql);
		List<Main> list = query.list();
		model.addAttribute("combo",list);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
//		for(int i=0;i<list.size();i++){
//			System.out.println(list.get(i).getName());
//		}
//		
		return "tab/combo";
	}
	
	@RequestMapping("sanpham")
	public String sanpham(HttpServletRequest rq,ModelMap model){
		
		String x = rq.getParameter("id");
		
		Session open = factory.openSession();
		Transaction b = open.beginTransaction();
		try {
			String hql = "FROM Main where id = "+Integer.parseInt(x)+"";
			Query query = open.createQuery(hql);
			List<Main> list = query.list();
			model.addAttribute("sanpham",list);
		//	System.out.println(list.get(0).getImg());
			
		} catch (Exception e) {
			// TODO: handle exception
			b.rollback();
			model.addAttribute("dlt",0);
			
		}
		finally {
			open.close();
		}
		
		return "tab/detail";
	}
	
	
	
	@RequestMapping(value="admin",method=RequestMethod.GET)
	public String admin(){
		return "admin/login";
	}
	public int fnid=0;
	public int fnsl=0;
	@RequestMapping(value="very",method=RequestMethod.POST)
	public String very(ModelMap model,HttpServletRequest rq){
		String x = rq.getParameter("key");
		int soluong =Integer.parseInt( rq.getParameter("soluong"));
		String hql ="select amount from Main where id = "+x+"";
		Session session = factory.getCurrentSession();
		Query query1 = session.createQuery(hql);
		int amount = (int) query1.uniqueResult();
		
		if(amount-soluong >=0){
			fnid=Integer.parseInt(x);
			fnsl=soluong;
			return "tab/cmnd";
		}else{
			model.addAttribute("thongbao","1");
			//	System.out.println(amount);
				return "tab/detail";
		}
	
	}
	public int check=0;
	public String fncmnd="";
	@RequestMapping(value="cmnd",method=RequestMethod.POST)
	public String cmnd(ModelMap model,HttpServletRequest rq){
		String cmnd = rq.getParameter("cmnd");
		Session open = factory.openSession();
		Transaction b = open.beginTransaction();
		try {	
		//System.out.println(cmnd);
		String hql = "from Customer";
		Query query = open.createQuery(hql);
		List<Customer> list = query.list();
		for(int i=0;i<list.size();i++){
		//	System.out.println(list.get(i).getCmnd());
			if(cmnd.equals(list.get(i).getCmnd())){
				model.addAttribute("infomation",list.get(i));
				check=1;
				fncmnd=list.get(i).getCmnd();
				return "tab/thanhtoan";
		}
			}
		} catch (Exception e) {
			
			b.rollback();
			System.out.println(e);
		
		}
		finally {
			open.close();
		}
		check=0;
		model.addAttribute("infomation",new Customer());
		return "tab/thanhtoan";
		}
	public String fnname="";
	public int fngia=0;
	public String fnemail="";
	@RequestMapping(value="thanhtoan",method=RequestMethod.POST)
	public String thanhtoan(ModelMap model,@ModelAttribute("infomation")Customer customers ){
		 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");  
		 LocalDateTime now = LocalDateTime.now();
		Session open = factory.openSession();
		Transaction b = open.beginTransaction();
		if(check==1){
			try {
				String hql = "from Main where id = "+fnid+"";
				Query query1 = open.createQuery(hql);
				Main spham =(Main)  query1.uniqueResult();
			
				String hql2 = "from Customer where cmnd="+fncmnd+"";
				Query query2 = open.createQuery(hql2);
				Customer x = (Customer) query2.uniqueResult();
				Oder temp = new Oder();
				fnname = spham.getName()+"- So luong x"+fnsl;
				temp.setName(fnname);
				fngia=fnsl*spham.getPrice();
				temp.setPrice(fngia);
				fnemail=x.getEmail();
				temp.setCustomer(x);
				temp.setDate(dtf.format(now));
				open.save(temp);
				spham.setAmount(spham.getAmount()-fnsl);
				open.update(spham);
				b.commit();
				

				//System.out.println(spham.getName());
				
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);
			}
			
		}else{
			try {
				String hql = "from Main where id = "+fnid+"";
				Query query1 = open.createQuery(hql);
				Main spham =(Main)  query1.uniqueResult();
			
				Oder temp = new Oder();
				fnname=spham.getName()+"- So luong x"+fnsl;
				temp.setName(fnname);
				fngia=fnsl*spham.getPrice();
				temp.setPrice(fngia);
				fnemail=customers.getEmail();
				temp.setCustomer(customers);
				temp.setDate(dtf.format(now));
				open.save(temp);
				spham.setAmount(spham.getAmount()-fnsl);
				
				open.save(customers);
				open.update(spham);
				b.commit();
				

				//System.out.println(spham.getName());
				
			} catch (Exception e) {
				System.out.println(e);
			}
			
		}
		
		try {
			MimeMessage mail = mailer.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mail);
			helper.setFrom("HauStore","HauStore");
			helper.setTo(fnemail);
			helper.setReplyTo("HauStore","HauStore");
			helper.setSubject("Xác nhận đơn hàng của bạn !");
			helper.setText("Đơn hàng của bạn gồm : "+fnname+", Có giá tiền là : "+fngia+". Cảm ơn bạn vì đã mua hàng của chúng tôi",true);
			mailer.send(mail);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
	//	System.out.println(fnname+" "+fngia+" "+fnemail);
		model.addAttribute("name1",fnname);
		model.addAttribute("gia1",fngia);
		model.addAttribute("mail1",fnemail);
		open.close();
		return "tab/finaly";
		
	}
	@RequestMapping("xacnhan")
	public String xacnhan(ModelMap model){
		System.out.println(fnname+" "+fngia+" "+fnemail);
		model.addAttribute("name1",fnname);
		model.addAttribute("gia1",fngia);
		model.addAttribute("mail1",fnemail);
		
		return "tab/finaly";
	}
	@RequestMapping("send")
	public String send(ModelMap model){
		try {
			MimeMessage mail = mailer.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mail);
			helper.setFrom("HauStore","HauStore");
			helper.setTo("nevermorethuan1408@gmail.com");
			helper.setReplyTo("HauStore","HauStore");
			helper.setSubject("Ken dep trai");
			helper.setText("zzzzz",true);
			mailer.send(mail);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "tab/finaly";
	}
		
		
	

}

