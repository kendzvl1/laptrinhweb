package poly.controller;

import java.io.File;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.sun.xml.internal.bind.CycleRecoverable.Context;

import poly.entity.Customer;
import poly.entity.Info;
import poly.entity.Main;
import poly.entity.Oder;
import poly.entity.User;

@Controller
@Transactional
public class login1Controller {
	@Autowired
	SessionFactory factory;
	@Autowired
	ServletContext context;
	
	
	@RequestMapping(value="login1",method=RequestMethod.POST)
	public String login(HttpServletRequest request , ModelMap model){
		
		Session session = factory.getCurrentSession();
		String hql = "FROM User";
		Query query = session.createQuery(hql);
		List<User> list = query.list();
		String user = request.getParameter("name");
		String pass = request.getParameter("pass");
			String b="";	
		int flag=1;			
		for (User x : list) {
			if(x.getUsername().equals(user)&& x.getPass().equals(pass)){
				//model.addAttribute("message",x.getUsername());
				b=x.getUsername();
				flag=0;
			}
		}
		if(flag==0){
			List<Main> list2 = getMain();
			model.addAttribute("main",list2);
			HttpSession ss = request.getSession();
			ss.setAttribute("username", b);
			model.addAttribute("name",b);
			return "admin/table";
			
		}else{
			model.addAttribute("vl",1);
			return "admin/login";
		}
	}
	
	@RequestMapping("logout")
	public String logout(HttpServletRequest rq){
		HttpSession ss = rq.getSession();
		ss.removeAttribute("username");
		return "admin/login";
	}

	
	@RequestMapping(value="insert",method=RequestMethod.GET)
		public String insert(ModelMap model,HttpServletRequest rq){
		HttpSession ss = rq.getSession();
		Object obj = ss.getAttribute("username");
		String name="";
		if(obj!=null){
			name = (String) obj;
			model.addAttribute("name",name);
			model.addAttribute("main",new Main());
			return "admin/form";
			
		}else{
			model.addAttribute("vl",1);
			return "admin/login";
		}
		
		}
	@RequestMapping(value="table",method=RequestMethod.GET)
	public String table(ModelMap model,HttpServletRequest rq){
		HttpSession ss = rq.getSession();
		Object obj = ss.getAttribute("username");
		String name="";
		if(obj!=null){
			name = (String) obj;
			model.addAttribute("name",name);
			List<Main> list2 = getMain();
			model.addAttribute("main",list2);
			return "admin/table";
			
		}else{
			model.addAttribute("vl",1);
			return "admin/login";
		}
		
		
		
		
	}
	@RequestMapping(value="insert", method=RequestMethod.POST)
	public String insert2(ModelMap model,@RequestParam("photo") MultipartFile photo,@ModelAttribute("main")Main main,BindingResult erros){
		
		if(main.getName().trim().length() < 3){
			erros.rejectValue("name", "Main","Nhập tên sản phẩm dài hơn !");
		}
		if(main.getDescription().trim().length() < 5){
			erros.rejectValue("description", "Main","Nhập mô tả dài hơn !");
		}
		if(main.getAmount()<1){
			erros.rejectValue("amount", "Main","Số lượng không được nhỏ hơn 1 !");
		}
		if(main.getPrice()<1){
			erros.rejectValue("price", "Main","Giá tiền không được âm !");
		}
		if(erros.hasErrors()){
			
		}else{
		if(photo.isEmpty()){
			model.addAttribute("add",2);
		}else{
		
		Session session = factory.openSession();
		Transaction t = session.beginTransaction();
		
		try {
			String patch = "C:/Users/Admin/workspace/Doanweb/WebContent/files/"+photo.getOriginalFilename() ;	
			//C:\Users\Admin\workspace\Doanweb\WebContent\files
			System.out.println(patch);
			photo.transferTo(new File(patch));
			main.setImg(photo.getOriginalFilename());
			session.save(main);
			t.commit();
			model.addAttribute("add",1);

			
		} catch (Exception e) {
			// TODO: handle exception
			t.rollback();
			model.addAttribute("add",0);
		}finally {
			session.close();
		}
		}
		model.addAttribute("main",new Main());
		}
		return "admin/form";
	}
	
	@RequestMapping(value ="delete",method=RequestMethod.GET)
	public String delete(HttpServletRequest rq,ModelMap model){
		Main us = new Main();
		String x = rq.getParameter("id");
		us.setId(Integer.parseInt(x));
		Session open = factory.openSession();
		Transaction b = open.beginTransaction();
		try {
			open.delete(us);
			b.commit();
			model.addAttribute("dlt",x);
			
			
			
		} catch (Exception e) {
			// TODO: handle exception
			b.rollback();
			model.addAttribute("dlt",0);
			
		}finally{
			List<Main> list2 = getMain();
			model.addAttribute("main",list2);
			open.close();
		}
		
		return "admin/table";
	
		
		
	}
	
	
	@ModelAttribute("infos")
	public List<Info> getdata(){
		Session session = factory.getCurrentSession();
		String hql = "FROM Info";
		Query query = session.createQuery(hql);
		List<Info> list = query.list();
		return list;
	}
	
	@RequestMapping(value="edit",method=RequestMethod.POST)
	public String ediit(ModelMap model,@RequestParam("photox") MultipartFile photo,@ModelAttribute("main")Main main){
		
		Session session = factory.openSession();
		Transaction t = session.beginTransaction();
		if(photo.isEmpty()){
		}else{
			try{
			String patch = "C:/Users/Admin/workspace/Doanweb/WebContent/files/"+photo.getOriginalFilename() ;	
			photo.transferTo(new File(patch));
			main.setImg(photo.getOriginalFilename());
			}catch(Exception e){
				
			}
		}
		try {
			
			session.update(main);
			t.commit();
			model.addAttribute("ed",1);
			
		} catch (Exception e) {
			t.rollback();
			model.addAttribute("ed",0);
		}
		finally{
			session.close();
		}
		return "admin/edit";
		
	}
	

	@RequestMapping("{id}")
	public String edit(ModelMap model,@PathVariable("id") String id){
		Session session = factory.getCurrentSession();
		Main main = (Main) session.get(Main.class,Integer.parseInt(id) );
		//System.out.println(main.getImg());
		model.addAttribute("main",main);
		
		return "admin/edit";
		
	}
	
	@RequestMapping("date")
	public String date(HttpServletRequest rq,ModelMap model){
		
		HttpSession ss = rq.getSession();
		Object obj = ss.getAttribute("username");
		String name="";
		if(obj!=null){
			name = (String) obj;
			model.addAttribute("name",name);
			return "admin/date";
			
		}else{
			model.addAttribute("vl",1);
			return "admin/login";
		}
		
		
	}
	@RequestMapping(value="checkdate",method=RequestMethod.POST)
	public String datep(HttpServletRequest rq , ModelMap model){
		try {
			String day = rq.getParameter("day");
			
			String hql = "From Oder where date = '"+day+"'";
			Session sesion = factory.getCurrentSession();
			Query query =  sesion.createQuery(hql);
			List<Oder> list = query.list();
			
			if(list.size()>0){
				model.addAttribute("ds",list);
				model.addAttribute("nof",list.size());
			}else{
				model.addAttribute("nof",0);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return "admin/date";
	}
	
	@RequestMapping("search")
	public String search(ModelMap model,HttpServletRequest rq){
		
		HttpSession ss = rq.getSession();
		Object obj = ss.getAttribute("username");
		String name="";
		if(obj!=null){
			name = (String) obj;
			model.addAttribute("name",name);
			return "admin/search";
			
		}else{
			model.addAttribute("vl",1);
			return "admin/login";
		}
		
	}
	@RequestMapping(value="cmndcheck",method=RequestMethod.POST)
	public String search1(HttpServletRequest rq,ModelMap model){
		try {
			
		
		String xcmnd = rq.getParameter("scmnd");
		String hql = "From Customer where cmnd = "+xcmnd+"";
		Session ss = factory.getCurrentSession();
		Query query =  ss.createQuery(hql);
		List<Customer> list = query.list();
		if(list.size()>0){
			model.addAttribute("dx",list);
			model.addAttribute("noff",list.size());
		}else {
			model.addAttribute("noff",0);
		}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return "admin/search";
	}
	
//	@SuppressWarnings("unchecked")
//	public List<Main> geUsers(){
//		Session session = factory.getCurrentSession();
//		String hql = "FROM Main";
//		Query query = session.createQuery(hql);
//		List<Main> list = query.list();
//		return list;
//	}
	@SuppressWarnings("unchecked")
	public List<Main> getMain(){
		Session session = factory.getCurrentSession();
		String sq = "FROM Main";
		Query query2 = session.createQuery(sq);
		List<Main> list2 = query2.list();
		return list2;
		
	}
	

	
}
