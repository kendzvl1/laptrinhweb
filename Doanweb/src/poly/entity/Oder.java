package poly.entity;



import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

@Entity
@Table(name="Oders")
public class Oder {
	@Id @GeneratedValue
	private int idoder;
	private String name;

	private int price;
	
	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	private String date;

	@ManyToOne
	@JoinColumn(name="cmnd")
	private Customer customer;

	public int getIdoder() {
		return idoder;
	}

	public void setIdoder(int idoder) {
		this.idoder = idoder;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	

}
