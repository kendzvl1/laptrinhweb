<%@ page pageEncoding = "utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<!DOCTYPE html >
<html>
<head>
<title>Searching</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="./css/new.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
  <script src="https://kit.fontawesome.com/990909bc49.js"></script>
   <link rel="stylesheet" href="./css/toastr.min.css">
<base href="${pageContext.servletContext.contextPath}/">

</head>

<body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <div class="navbar-brand">Admin Control</div>
    </div>
   <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Hello ${name}</a></li>
        <li><a href="logout.htm"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
      </ul>
 
</nav>

<div class="menu">
    
   
 


    
            <ul class="cha">
                <li><i class="fas fa-tachometer-alt"></i></li>
         		<li > <a href="table.htm"><i class="fas fa-table"></i>Tổng Quan</a></li>
                <li ><a href="insert.htm"><i class="fas fa-plus-square"></i>Thêm Sản Phẩm</a></li>
                <li ><a href="date.htm"><i class="fas fa-search"></i>Tra Cứu Đơn Hàng</a></li>
                <li class="active"><a href="search.htm"><i class="fas fa-chalkboard-teacher"></i>Thông Tin Khách Hàng</a></li>

            </ul>
        </div>
 

   
				<div class="phom">
                          
                          <h3 class="text-center">Nhập Số CMND Khách Hàng Muốn Tìm</h3>
							<form action="./cmndcheck.htm" method="post">
							
							
							<div class="input-group">
							<label for="basic-url">Số CMND :</label>
							 
							  <input type="number" class="form-control" placeholder="215437050" name="scmnd" required="required">
							</div>
							
							<div class="input-group">
							  <button type="submit" class="btn btn-success " style="width: 200%" >
							    Check 
							  </button>
							  </div>
							</form>
							
							
					<table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">CMND</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Phone Number</th>
                                            <th scope="col">Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="u" items="${dx}"> 
                                        <tr>
                                            <th scope="row">${u.cmnd}</th>
                                            <td>${u.name}</td>
                                            <td>${u.email}</td>
                                            <td>${u.numberphone}</td>
                                            <td>${u.address}</td>
                                        </tr>
                                       </c:forEach>
                                    </tbody>
                                </table>			


</div>

<script src="./css/toastr.min.js"></script>
	<script>
	toastr.options = {
			  "closeButton": true,
			  "debug": false,
			  "newestOnTop": false,
			  "progressBar": true,
			  "positionClass": "toast-top-right",
			  "preventDuplicates": false,
			  "onclick": null,
			  "showDuration": "300",
			  "hideDuration": "1000",
			  "timeOut": "5000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}
		var x=${noff}
		
			
		if(x==0){
			toastr["error"]("Số CMND không tồn tại !");
		}else{
			toastr["success"]("Tìm thấy !");
		}
		
	
			
		
	</script>

</body>
</html>