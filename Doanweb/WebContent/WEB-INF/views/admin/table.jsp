<%@ page pageEncoding = "utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="f" %>

<!DOCTYPE html >
<html>
<head>
<title>Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="./css/new.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
  <script src="https://kit.fontawesome.com/990909bc49.js"></script>
   <link rel="stylesheet" href="./css/toastr.min.css">
<base href="${pageContext.servletContext.contextPath}/">

</head>
<body>

	<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <div class="navbar-brand" >Admin Control</div>
    </div>
  <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Hello ${name}</a></li>
        <li><a href="logout.htm"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
      </ul>
 
</nav>

<div class="menu">
    
   
           <ul class="cha">
                <li><i class="fas fa-tachometer-alt"></i></li>
                <li class="active"> <a href="table.htm"><i class="fas fa-table"></i>Tổng Quan</a></li>
                <li><a href="insert.htm"><i class="fas fa-plus-square"></i>Thêm Sản Phẩm</a></li>
                <li><a href="date.htm"><i class="fas fa-search"></i>Tra Cứu Đơn Hàng</a></li>
                <li><a href="search.htm"><i class="fas fa-chalkboard-teacher"></i>Thông Tin Khách Hàng</a></li>

            </ul>
    </div>
    
    
    
	<div class="phom2">
								<table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Image</th>
                                            <th scope="col">Price</th>
                                            <th scope="col">Type</th>
                                            <th scope="col">Amount</th>
                                            <th scope="col">Edit</th>
                                            <th scope="col">Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="u" items="${main}"> 
                                        <tr>
                                            <th scope="row">${u.id}</th>
                                            <td>${u.name}</td>
                                            <td><img src="./files/${u.img}" width="50px"> </td>
                                            <td><f:formatNumber minFractionDigits="0" value="${u.price}" > </f:formatNumber> đ </td>
                                            <td>${u.info.name}</td>
                                            <td>${u.amount}</td>
                                            <td><a href="./${u.id}.htm">Edit</a></td>
                                            <td><a href="./delete.htm?id=${u.id}">Delete</a></td>
                                        </tr>
                                       </c:forEach>
                                    </tbody>
                                </table>
	</div>
	
	
	<script src="./css/toastr.min.js"></script>
	<script>
	toastr.options = {
			  "closeButton": true,
			  "debug": false,
			  "newestOnTop": false,
			  "progressBar": true,
			  "positionClass": "toast-top-right",
			  "preventDuplicates": false,
			  "onclick": null,
			  "showDuration": "300",
			  "hideDuration": "1000",
			  "timeOut": "5000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}
		var x=${dlt}
	
		if(x==0){
			toastr["error"]("Xóa Thất Bại !");
		}else{
			toastr["success"]("Đã xóa thành công id : "+x);
		}
		
	
			
		
	</script>
	
</body>
</html>