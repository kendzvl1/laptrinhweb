<%@ page pageEncoding = "utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<!DOCTYPE html >
<html>
<head>
<title>Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="./css/1.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
  <script src="https://kit.fontawesome.com/990909bc49.js"></script>

<base href="${pageContext.servletContext.contextPath}/">

</head>
<body>

<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
           <div class="row">
            <ul class="menu ">
            
                <!-- <p class="logo">SIMPLE LOGO</p> -->
               <a href="#"><img src="./img/logo.png" ></a> 
                <div class="bo">
                    
                    </div>
                <li class="btnn mot" ><i class="fas fa-plus-circle"></i><a href="insert.htm">Insert</a></li>
                <li class="btnn hai"><i class="far fa-edit"></i><a href="#">Edit</a></li>
                <li class="btnn ba"><i class="far fa-trash-alt"></i><a href="#">Delete</a></li>
                <li class="btnn"><a href="#">About</a></li>
                <li class="btnn"><a href="#">About</a></li>
            </ul>
           </div>
       </div>
            
 <div class="header">

  <div class="from-header ">
    <input type="text" placeholder="Search.." name="search">
      <button  type="submit"><i class="fa fa-search"></i></button>
  </div>
  <ul class="nav navbar-nav navbar-right">
    <li><a href="#"><span class="glyphicon glyphicon-user"></span> Hello ${message} ! </a></li>
    <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
  </ul>
 </div>       
    

<!-- form -->

<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
    <div class="row">
        <div class="content">
            

            <div class="phom2" id="id2">
                    <form action="getitems.htm" >
                        <h2>Chose Items You Want</h2>
                        <div class="input-container">
                          <i class="fa fa-user icon"></i>
                          <form:select path="it" items="${it}" itemValue="idtype" itemLabel="name" name="ab"></form:select>
                        </div>
                      
                      
                      
                        <button type="submit" class="btn">GET</button>
                      </form>
                    </div>
                    
                    
    <div class="tab" id="tabx">
          <div class="panel panel-primary">
              <div class="panel-heading">Quản lý kho hàng</div>
              <table class="table table-bordered panel-body">
                  <thead>
                    <tr>
                    <th>ID</th>
                      <th>Name</th>
                      <th>Image</th>
                      <th>Price</th>
                      <th>Amount</th>
                      <th>Delete</th>
                      <th>Edit</th>
                    </tr>
                  </thead>
                  <tbody>
                  <c:forEach var="u" items="${main}"> 
                    <tr>
                      <td>${u.id}</td>
                      <td>${u.name}</td>
                      <td>${u.img}</td>
                      <td>${u.price}</td>
                      <td>${u.amount}</td>
                      
                      <td><a href="#">link</a></td>
                      <td><a href="#">link</a></td>
                    </tr>                                
                  </c:forEach>
                  </tbody>
                </table>
              </div>
            </div> 

        </div>
   </div>
</div>



    



<script>
        $( ".btnn" ).click(function() {
            $(".btnn").removeClass("active");
      $(this).addClass("active");
    });
</script>

<script>  
     $( ".mot" ).click(function() {
      $(".phom2").css("display","none");
      $(".tab").css("display","none");
      $("#idd").toggle("slow");
    });
 
</script>

<script>  
  $( ".hai" ).click(function() {
    $(".phom").css("display","none");
    $(".tab").css("display","none");
    $("#id2").toggle("slow");
 });
</script>

<script>  
    $( ".ba" ).click(function() {
      $(".phom").css("display","none");
      $(".phom2").css("display","none");
      $("#tabx").toggle("slow");
   });
  
 </script>

</body>
</html>