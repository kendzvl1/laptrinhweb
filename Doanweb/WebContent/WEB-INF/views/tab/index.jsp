 <%@ page pageEncoding = "utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="f" %>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
     <base href="${pageContext.servletContext.contextPath}/"> 
    <title>Mua tivi, máy tính, laptop đến Phong Vũ gần nhất</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/990909bc49.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./css/11.css">


</head>

<body>


    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">

                <a class="navbar-brand" href="./trangchu.htm">

                </a>

            </div>

            <form class="navbar-form navbar-left" action="./action_page.htm" method="get">
                <div class="input-group">
                    <input required="required" type="text" class="form-control"
                        placeholder="Nhập tên sản phẩm, mã sản phẩm, từ khóa cần tìm..." name="xsearch" size="50" 
                        />
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>

            <ul class="nav navbar-nav">
                <li>
                    <i class="fas fa-percent"></i>
                    <a href="admin.htm">
                        <p>Chương trình</p>
                        <p>khuyến mãi</p>
                    </a></li>
                <li>
                    <i class="fas fa-user-circle"></i>
                    <a href="admin.htm">
                        <p>Đăng Nhập</p>
                        <p>Đăng Kí</p>

                    </a></li>
                <li>
                    <i class="fas fa-shipping-fast"></i>
                    <a href="admin.htm">
                        <p>Kiểm tra</p>
                        <p>đơn hàng</p>
                    </a></li>
            </ul>

        </div>
    </nav>
    <!-- xong phan header tren -->

    <div class="duoi">
        <nav class="navbar navbar-inverse">

            <div class="container">
                <div class="row">

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">

                    </div>

                    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="#">
                                    <div class="ga">
                                        <i class="fas fa-chevron-down"></i>
                                        <p>Sản phẩm vừa xem</p>

                                    </div>
                                </a>
                            </li>
                            <li>

                                <a href="#">
                                    <div class="ga">
                                        <i class="far fa-building"></i>
                                        <p>Hệ Thống Showroom</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="ga">
                                        <i class="fas fa-headset"></i>
                                        <p>Tổng đài miễn phí</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="ga">
                                        <i class="fas fa-tools"></i>
                                        <p>Kiểm tra bảo hành</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="ga">
                                        <i class="far fa-bell"></i>
                                        <p>Thông báo</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="ga">
                                        <i class="fas fa-wrench"></i>
                                        <p>Xây dựng cấu hình</p>
                                    </div>
                                </a>
                            </li>



                        </ul>
                    </div>


                </div>
            </div>
        </nav>
    </div>



    <!-- khoi quang cao -->
    <div class="khoiqc">
        <div class="container">
            <div class="row">

                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <div class="menutrai">
                        <ul class="menudoc">
                            <li class="pathen"><a href="main.htm">Bo mạch chủ</a>

                            </li>
                            <li><a href="vga.htm">Card màn hình - VGA</a></li>
                            <li><a href="chip.htm">Vi xử lý - CPU</a></li>
                            <li><a href="psu.htm">Nguồn - PSU</a></li>
                            <li><a href="ram.htm">Ram</a></li>
                            <li><a href="case.htm">Vỏ Case - Thùng máy tính</a></li>
                            <li><a href="ocung.htm">Ổ cứng</a></li>
                            <li><a href="gear.htm">Linh kiện máy tính</a></li>
                            <li><a href="gear.htm">Gaming gear & Phụ kiện</a></li>
                            <li><a href="combo.htm">Combo - Máy Bàn</a></li>
                            <li><a href="gear.htm">Thiết bị văn phòng</a></li>
                            <li><a href="gear.htm">Thiết bị an ninh</a></li>
                            <li><a href="gear.htm">Phần mềm - thiết bị mạng</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">

                    <div class="slide">

                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                <li data-target="#myCarousel" data-slide-to="2"></li>
                                <li data-target="#myCarousel" data-slide-to="3"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="img/1.jpg" alt="Chicago" style="width:680px; height: 400px;">
                                </div>

                                <div class="item">
                                    <img src="img/2.jpg" alt="Chicago" style="width:680px; height: 400px;">
                                </div>

                                <div class="item">
                                    <img src="img/3.jpg" alt="New york" style="width:680px; height: 400px;">
                                </div>

                                <div class="item">
                                    <img src="img/4.jpg" alt="New york" style="width:680px; height: 400px;">
                                </div>
                            </div>

                            <!-- Left and right controls -->


                        </div>


                    </div>

                    <div class="buton">
                        <a class="bt" href="#myCarousel" data-slide-to="0">Back To School</a>
                        <a class="bt" href="#myCarousel" data-slide-to="1">Sale 24/7</a>
                        <a class="bt" href="#myCarousel" data-slide-to="2">Thương hiệu uy tín</a>
                        <a class="bt" href="#myCarousel" data-slide-to="3">Smart Phone </a>
                    </div>

                </div>




                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="menuphai">
                        <div class="row">
                            <div class="anh">

                            </div>
                            <div class="anh1">

                            </div>
                            <div class="anh2">

                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>







    <div class="container">
        <div class="text-center">
        <h3 >COMBO CHẤT - GIÁ BAO SỐC</h3>
        <p>Ưu đãi dành riêng cho sinh viên !</p>
    </div>



        <div class="main">
            <div class="row">

                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <div class="htrai">
	<a href="./sanpham.htm?id=${combo[0].id}">
                        <div class="giovang">
                         
                                <div class="sale">
                                    <i class="phantram">-7%</i>
                                    <i class="fas fa-gift ic"></i>
                                </div>
                                <div class="spham">
                                    <img src="./files/${combo[0].img}" alt="">
                                </div>
                                <div class="info">
                                    <p >
                                        ${combo[0].name}
                                    </p>
                                </div>
                                <div class="price">
                                <span><f:formatNumber minFractionDigits="0" value="${combo[0].price}" > </f:formatNumber> đ </span>
                                   
                                    <p>17.999.000đ</p>
                                </div>
                            
                        </div>
</a>
                        <div class="danhmuc">
                            <h4>DANH MỤC</h4>
                            <ul>
                                <li class="tc">Tất cả</li>
                                <li>Laptop Gaming</li>
                                <li>Laptop sinh viên - văn phòng</li>
                                <li>Laptop 2 trong 1</li>
                                <li>Laptop đồ họa</li>
                                <li>Laptop mỏng nhẹ</li>

                            </ul>
                        </div>


                    </div>
                </div>


                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                    <div class="hphai">
						<c:forEach var="p" items="${combo}" begin="1" end="8" >
						<a href="./sanpham.htm?id=${p.id}">
                        <div class="giovang">
                            
                                <div class="sale">
                                    <i class="phantram">-7%</i>
                                    <i class="fas fa-gift ic"></i>
                                </div>
                                <div class="spham">
                                    <img src="./files/${p.img}" alt="">
                                </div>
                                <div class="info">
                                    <p>
                                        ${p.name}
                                    </p>
                                </div>
                                <div class="price">
                                    <span><f:formatNumber minFractionDigits="0" value="${p.price}" > </f:formatNumber> đ </span>
                                    <p>17.999.000đ</p>
                                </div>
                            
                        </div>
                        </a>
</c:forEach>
                       

                    </div>
                </div>


            </div>
        </div>
    </div>
    <div class="container">

        <div class="bt text-center">


            <a href="combo.htm">Xem tất cả</a>

        </div>
    </div>



<div class="container">
    <h3><i class="fas fa-microchip"></i>CPU - Chip xử lí</h3>
</div>
        

 

    <div class="container">
        <div class="shock">

     	    <c:forEach var="p" items="${cpu}" begin="0" end="4">
                <div class="giovang">
                    <a href="./sanpham.htm?id=${p.id}">
                        <div class="sale">
                            <i class="phantram">-7%</i>
                            <i class="fas fa-gift ic"></i>
                        </div>
                        <div class="spham">
                           <img src="./files/${p.img}" alt="">
                        </div>
                        <div class="info">
                             ${p.name}
                             
                        </div>
                        <div class="price">
                            <span><f:formatNumber minFractionDigits="0" value="${p.price}" > </f:formatNumber> đ </span>
                            <p>5.999.000đ</p>
                        </div>
                    </a>
                </div>

                
    </c:forEach>




            
        </div>

    </div>



    <div class="container">
        <h3><i class="fas fa-memory"></i> Ram</h3>
    </div>
            
    
     
    
        <div class="container">
            <div class="shock">
    <c:forEach var="p" items="${ram}" begin="0" end="4">
                <div class="giovang">
                    <a href="./sanpham.htm?id=${p.id}">
                        <div class="sale">
                            <i class="phantram">-7%</i>
                            <i class="fas fa-gift ic"></i>
                        </div>
                        <div class="spham">
                           <img src="./files/${p.img}" alt="">
                        </div>
                        <div class="info">
                             ${p.name}
                        </div>
                        <div class="price">
                            <span><f:formatNumber minFractionDigits="0" value="${p.price}" > </f:formatNumber> đ </span>
                            <p>3.999.000đ</p>
                        </div>
                    </a>
                </div>
    
    
    
                
    </c:forEach>
                
            </div>
    
        </div>




        <div class="container">

            <div class="vien">
            
              <div class="footer">
                <div class="top">
                    <div class="">
                      <div class="khoi text-center">
                        <i class="fas fa-truck-pickup"></i>
                        <SPAN> MIỄN PHÍ VẬN CHUYỂN </SPAN>
                      </div>
                      <div class="khoi text-center">
                        <i class="fas fa-headphones-alt"></i>
                        <SPAN> HỖ TRỢ 24/7</SPAN>
                      </div>
                      <div class="khoi text-center hidez">
                        <i class="fas fa-user-shield"></i>
                        <SPAN> THANH TOÁN BẢO MẬT </SPAN>
                      </div>
                      <div class="khoi khoip hidez">
                          <i class="fas fa-envelope-open-text" style="padding-left: 10px; color: #aaa;"></i>
                        <strong> Đăng ký nhận chương trình khuyến mãi mỗi ngày!</strong>
                        <div class="v">
                          <input type="text" size="30">
                          
                          <button type="button" class="btn btn-success">Đăng kí</button>
                          
                        </div>
                      </div>
                    </div>
                </div>
                <div class="bot">
                  <div class="">
                     
            
                  <div class="duoi1">
                      <div class="row">
            
                        <div class="khoii1">
                          <p>Hỗ trợ khách hàng</p>
            
                          <ul>
                            <li>Thẻ ưu đãi</li>
                            <li>Phiếu mua hàng</li>
                            <li>Trung tâm bảo hành</li>
                            <li>Thanh toán và giao hàng</li>
                            <li>Dịch vụ sửa chữa và bảo trì</li>
                          </ul>
                        </div>
            
                        <div class="khoii1">
                            <p>Chính sách Mua hàng và Bảo hành</p>
              
                            <ul>
                              <li>Quy định chung</li>
                              <li>Chính sách Bảo mật Thông tin</li>
                            
                              <li>Chính sách bảo hành</li>
                              <li>Chính sách trả góp</li>
                            </ul>
                          </div>
            
                          <div class="khoii1">
                              <p>Thông tin Phong Vũ</p>
                
                              <ul>
                                <li>Thông tin liên hệ</li>
                                <li>Hệ thống Showroom</li>
                                <li>Giới thiệu </li>
                                <li>Hỏi đáp</li>
                                <li>Tin công nghệ</li>
                              </ul>
                            </div>
            
                            <div class="khoii1">
                                <p>Cộng đồng Phong Vũ</p>
                  
                                <ul>
                                  <li> <i class="fab fa-facebook-square"></i>FaceBook Việt Nam</li>
                                  <li><i class="fab fa-youtube"></i>Youtube Media</li>
                                  <li><i class="fas fa-phone-square-alt"></i>Gọi mua hàng:1800 6867</li>
                                  <li><i class="fas fa-phone-square-alt"></i>Gọi chăm sóc:18006865</li>
                                  <li><i class="far fa-comments"></i>Chat với tư vấn viên</li>
                                </ul>
                              </div>
            
            
                              <div class="khoii1">
                                  <p>Hỗ trợ khách hàng</p>
                    
                                  <ul>
                                    <li><img src="https://phongvu.vn/media/wysiwyg/phongvu/phongvu_v3/da-dang-ky.png" alt="" style=""></li>
                                    <li>HTKH : abcxyz@gmail.com</li>
                                    <li>Liên hệ : kendeptrai@gmail.com</li>
                                   
                                  </ul>
                                </div>
            
            
                  
                        
            </div>
            </div>      
            </div>       
            
            
            </div>
                  <div class="duoi2">
                      <div class="">
                      <div class="pttt">
                        <p style="font-weight:700; ">Phương thức thanh toán</p>
                        <div class="ii">
                            <i class="fas fa-qrcode"></i>
                            <p>QR Code</p>
                        </div>
                        <div class="ii">
                            <i class="fas fa-money-bill"></i>
                            <p>Tiền mặt</p>
                        </div>
                        <div class="ii">
                            <i class="fas fa-hand-holding-usd"></i>
                            <p>Trả góp</p>
                        </div>
                        <div class="ii">
                            <i class="fas fa-mobile-alt"></i>
                            <p>Banking</p>
                        </div>
                      </div>
                    
                      <div class="pttt">
            
                        <p style="font-weight:700; ">Danh sách các ngân hàng thanh toán Online</p>
                        <img src="https://phongvu.vn/media/wysiwyg/phongvu/phongvu_v3/banklist.jpg" alt="thanhtoan">
                      </div>
                      
                    </div>
                    
                    </div>
            
            
            
            
                </div>
                </div>
            
             
                <div class="duoi3">
                  <div class=""> 
                    <a href="#">Tivi & Thiết bị thông minh / </a>   <a href="#"> PC và linh kiện /</a>
                  </div>
                </div>
            
              <div class="">
                <div class="duoi4">
                  
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h3>CÔNG TY CỔ PHẦN THƯƠNG MẠI DỊCH VỤ PHONG VŨ</h3>
                    <p>© 1997 - 2019 Công Ty Cổ Phần Thương Mại - Dịch Vụ Phong Vũ / GPĐKKD số 0304998358 do Sở KHĐT TP.HCM cấp</p>
                  </div>
            
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    
                      <span>Văn phòng điều hành miền Bắc:</span>
                      <p>Tầng 6, Số 1 Phố Thái Hà, Phường Trung Liệt, Quận Đống Đa, Hà Nội</p>
                    
                    
                      <span>Văn phòng điều hành miền Nam:</span>
                      <p>Tầng 7, tòa nhà số 198 Nguyễn Thị Minh Khai, Phường 6, Quận 3, TP. Hồ Chí Minh</p>
                    
                    </div>
                  
                </div>
              </div>
              </div>

</body>

</html>