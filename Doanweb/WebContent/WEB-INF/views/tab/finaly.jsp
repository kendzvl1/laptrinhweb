 <%@ page pageEncoding = "utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %> 
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %> 
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Mua tivi, máy tính, laptop đến Phong Vũ gần nhất</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <script src="https://kit.fontawesome.com/990909bc49.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="./css/11.css" >
   <link rel="stylesheet" href="./css/toastr.min.css">
</head>
<body>


   <nav class="navbar navbar-default">
  <div class="container dai">
    <div class="navbar-header">
    
    <a class="navbar-brand" href="./trangchu.htm" >
        
    </a>

    <form class="navbar-form navbar-left dodai" action="./action_page.htm">
        <div class="input-group">
          <input required="required" type="text" class="form-control" placeholder="Nhập tên sản phẩm, mã sản phẩm, từ khóa cần tìm..." name="search" size="100%" style="  width: 300px;" >
          <div class="input-group-btn">
            <button class="btn btn-default" type="submit">
              <i class="glyphicon glyphicon-search"></i>
            </button>
          </div>
        </div>
      </form>
   
     

    <ul class="nav navbar-nav trai">
      <li>
        <i class="fas fa-percent"></i>
          <a href="admin.htm">
              <p>Chương trình</p>
              <p>khuyến mãi</p>
          </a></li>
      <li>
            <i class="fas fa-user-circle"></i>
          <a href="admin.htm">
                <p>Đăng Nhập</p>
              <p>Đăng Kí</p>
              
          </a></li>
      <li>
            <i class="fas fa-shipping-fast"></i>  
        <a href="admin.htm">
            <p>Kiểm tra</p>
            <p>đơn hàng</p>
        </a></li>
    </ul>
  </div>
  </div>
</nav>

<div class="duoi">
    <nav class="navbar navbar-inverse">
        
        <div class="container">
            <div class="row">
                
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    
                </div>
                
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                        <ul class="nav navbar-nav">
                                <li >
                                    <a href="#"><div class="ga">
                                        <i class="fas fa-chevron-down"></i>
                                      <p>Sản phẩm vừa xem</p>
    
                                    </div> </a>
                                </li>
                                <li>
                                    
                                    <a href="#">
                                      <div class="ga">
                                          <i class="far fa-building"></i>
                                          <p>Hệ Thống Showroom</p>
                                      </div>
                                    </a>
                                </li>
                                <li>
                                  <a href="#">
                                    <div class="ga">
                                        <i class="fas fa-headset"></i>
                                      <p>Tổng đài miễn phí</p>
                                    </div>
                                  </a>
                              </li>
                              <li>
                                <a href="#"><div class="ga">
                                    <i class="fas fa-tools"></i>
                                  <p >Kiểm tra bảo hành</p>
                                </div></a>
                            </li>
                            <li>
                              <a href="#"><div class="ga">
                                  <i class="far fa-bell"></i>
                                <p>Thông báo</p>
                              </div></a>
                          </li>
                          <li>
                            <a href="#"><div class="ga">
                                <i class="fas fa-wrench"></i>
                              <p>Xây dựng cấu hình</p>
                            </div></a>
                        </li>
    
    
    
                            </ul>
                </div>
                
        
    </div>
    </div>
    </nav>
    </div>

<!-- xong phan head -->


	<div class="container">
      <div class="result">
    <h2>Thông tin đơn hàng</h2>
    <h4>Tên : ${name1}</h4>
    <h4>Tổng tiền thanh toán: ${gia1}</h4>
    <p>Chúng tôi đã gửi email xác nhận đến địa chỉ <span class="mail">${mail1}</span> . Vui lòng kiểm tra hộp thư điện tử của bạn</p>
        <a href="./trangchu.htm">Trở lại trang chủ !</a>
	</div>

</div>



<div class="container">

  <div class="vien">
  
    <div class="footer">
      <div class="top">
          <div class="">
            <div class="khoi text-center">
              <i class="fas fa-truck-pickup"></i>
              <SPAN> MIỄN PHÍ VẬN CHUYỂN </SPAN>
            </div>
            <div class="khoi text-center">
              <i class="fas fa-headphones-alt"></i>
              <SPAN> HỖ TRỢ 24/7</SPAN>
            </div>
            <div class="khoi text-center hidez">
              <i class="fas fa-user-shield"></i>
              <SPAN> THANH TOÁN BẢO MẬT </SPAN>
            </div>
            <div class="khoi khoip hidez">
                <i class="fas fa-envelope-open-text" style="padding-left: 10px; color: #aaa;"></i>
              <strong> Đăng ký nhận chương trình khuyến mãi mỗi ngày!</strong>
              <div class="v">
                <input type="text" size="30">
                
                <button type="button" class="btn btn-success">Đăng kí</button>
                
              </div>
            </div>
          </div>
      </div>
      <div class="bot">
        <div class="">
           
  
        <div class="duoi1">
  
            <div class="hieu">
    
                <div class="motkhoi">
                    <h3>Hotline liên hệ</h3>
                    <div class="ndmotkhoi">
                        <p>Gọi mua hàng:1800 6867</p>
                        <p>Gọi chăm sóc:18006865</p>
                    </div>
                </div>
                
                <div class="motkhoi">
                    <h3>Cộng đồng</h3>
                    <div class="ndmotkhoi">
                        <p><i class="fab fa-facebook-square"></i>FaceBook Việt Nam</p> 
                        <p><i class="fab fa-youtube"></i>Youtube Media</p>
                        <p><i class="far fa-comments"></i>Chat với tư vấn viên</p> 
                    </div>
                </div>
                
                <div class="motkhoi">
                    <h3>Hỗ trợ khách hàng</h3>
                    <div class="ndmotkhoi">
                        <p>Thẻ ưu đãi</p>
                         <p>Phiếu mua hàng</p>
                        <p>Trung tâm bảo hành</p>
                        <p>Thanh toán và giao hàng</p>
                     <p>Dịch vụ sửa chữa và bảo trì</p>
                    </div>
                </div>
                
                <div class="motkhoi">
                    <h3>Chách sách mua hàng</h3>
                    <div class="ndmotkhoi">
                        <p>Chính sách bảo hành</p>
                        <p>Chính sách trả góp</p>
                    </div>
                </div>
                
  
                </div>
  
            <div class="row hidez">
  
              <div class="khoii1">
                <p>Hỗ trợ khách hàng</p>
  
                <ul>
                  <li>Thẻ ưu đãi</li>
                  <li>Phiếu mua hàng</li>
                  <li>Trung tâm bảo hành</li>
                  <li>Thanh toán và giao hàng</li>
                  <li>Dịch vụ sửa chữa và bảo trì</li>
                </ul>
              </div>
  
              <div class="khoii1">
                  <p>Chính sách Mua hàng và Bảo hành</p>
    
                  <ul>
                    <li>Quy định chung</li>
                    <li>Chính sách Bảo mật Thông tin</li>
                  
                    <li>Chính sách bảo hành</li>
                    <li>Chính sách trả góp</li>
                  </ul>
                </div>
  
                <div class="khoii1">
                    <p>Thông tin Phong Vũ</p>
      
                    <ul>
                      <li>Thông tin liên hệ</li>
                      <li>Hệ thống Showroom</li>
                      <li>Giới thiệu </li>
                      <li>Hỏi đáp</li>
                      <li>Tin công nghệ</li>
                    </ul>
                  </div>
  
                  <div class="khoii1">
                      <p>Cộng đồng Phong Vũ</p>
        
                      <ul>
                        <li> <i class="fab fa-facebook-square"></i>FaceBook Việt Nam</li>
                        <li><i class="fab fa-youtube"></i>Youtube Media</li>
                        <li><i class="fas fa-phone-square-alt"></i>Gọi mua hàng:1800 6867</li>
                        <li><i class="fas fa-phone-square-alt"></i>Gọi chăm sóc:18006865</li>
                        <li><i class="far fa-comments"></i>Chat với tư vấn viên</li>
                      </ul>
                    </div>
  
  
                    <div class="khoii1">
                        <p>Hỗ trợ khách hàng</p>
          
                        <ul>
                          <li><img src="https://phongvu.vn/media/wysiwyg/phongvu/phongvu_v3/da-dang-ky.png" alt="" style=""></li>
                          <li>HTKH : abcxyz@gmail.com</li>
                          <li>Liên hệ : kendeptrai@gmail.com</li>
                         
                        </ul>
                      </div>
  
  
        
              
  </div>
  </div>      
  </div>       
  
  
  </div>
        <div class="duoi2">
            <div class="">
            <div class="pttt">
              <p style="font-weight:700; ">Phương thức thanh toán</p>
              <div class="ii">
                  <i class="fas fa-qrcode"></i>
                  <p>QR Code</p>
              </div>
              <div class="ii">
                  <i class="fas fa-money-bill"></i>
                  <p>Tiền mặt</p>
              </div>
              <div class="ii">
                  <i class="fas fa-hand-holding-usd"></i>
                  <p>Trả góp</p>
              </div>
              <div class="ii">
                  <i class="fas fa-mobile-alt"></i>
                  <p>Banking</p>
              </div>
            </div>
          
            <div class="pttt">
  
              <p style="font-weight:700; ">Danh sách các ngân hàng thanh toán Online</p>
              <img src="https://phongvu.vn/media/wysiwyg/phongvu/phongvu_v3/banklist.jpg" alt="thanhtoan">
            </div>
            
          </div>
          
          </div>
  
  
  
  
      </div>
      </div>
  
   
      <div class="duoi3">
        <div class=""> 
          <a href="#">Tivi & Thiết bị thông minh / </a>   <a href="#"> PC và linh kiện /</a>
        </div>
      </div>
  
    <div class="">
      <div class="duoi4">
        
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <h3>CÔNG TY CỔ PHẦN THƯƠNG MẠI DỊCH VỤ PHONG VŨ</h3>
          <p>© 1997 - 2019 Công Ty Cổ Phần Thương Mại - Dịch Vụ Phong Vũ / GPĐKKD số 0304998358 do Sở KHĐT TP.HCM cấp</p>
        </div>
  
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          
            <span>Văn phòng điều hành miền Bắc:</span>
            <p>Tầng 6, Số 1 Phố Thái Hà, Phường Trung Liệt, Quận Đống Đa, Hà Nội</p>
          
          
            <span>Văn phòng điều hành miền Nam:</span>
            <p>Tầng 7, tòa nhà số 198 Nguyễn Thị Minh Khai, Phường 6, Quận 3, TP. Hồ Chí Minh</p>
          
          </div>
        
      </div>
    </div>
    </div>
  







</body>
</html>